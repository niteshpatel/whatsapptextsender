﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace WhatsAppTextSender
{
    static class Program
    {
        static void Main(string[] args)
        {
            TypeText(args[0]);
        }

        [DllImport("user32.dll")]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        private static void TypeText(string text)
        {
            Process[] processes = Process.GetProcessesByName("WhatsApp");
            var gui = processes.FirstOrDefault(p => p.MainWindowTitle.Equals("WhatsApp"));
            if (gui != null)
            {
                SetForegroundWindow(gui.MainWindowHandle);
                SendKeys.SendWait($"{text}~");
            }
        }
    }
}
